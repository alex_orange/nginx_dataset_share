"""
Profile for sharing blockstore datasets via http.
"""

import geni.portal as portal
import geni.rspec.pg as rspec


portal.context.defineParameter("dataset",
                               "URN of dataset to share",
                               portal.ParameterType.STRING, "dataset_urn")


params = portal.context.bindParameters()
request = portal.context.makeRequestRSpec()

node = request.RawPC("node")
node.hardware_type = "d710"

# Blockstore to share
iface = node.addInterface()
fsnode = request.RemoteBlockstore("fsnode", "/usr/share/nginx/html")
fsnode.dataset = params.dataset
fsnode.readonly = True

fslink = request.Link("fslink")
fslink.addInterface(iface)
fslink.addInterface(fsnode.interface)

fslink.best_effort = True
fslink.vlan_tagging = True


portal.context.printRequestRSpec()
